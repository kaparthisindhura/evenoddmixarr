import java.util.*;

public class EvenOddMixArray {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n,even=0,odd=0;

        n= Integer.parseInt(sc.nextLine());
        int[] arr = new int[n];
        String[] s = sc.nextLine().split(" ");

        for(int  i=0 ;i < s.length;i++) {
            arr[i] = Integer.parseInt(s[i]);
            if(arr[i]%2==0)
            {
                even=1;
            }
            else
            {
                odd=1;
            }
            if(even==1 && odd==1)
            {
                System.out.println("Mixed");
                return ;
            }
        }
        if(even==1)
        {
            System.out.println("Even");
        }
        else
        {
            System.out.println("Odd");
        }
    }
}
